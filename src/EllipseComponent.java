import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class EllipseComponent extends JComponent {

    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;

        //Draw
        int x = getWidth();
        int y = getHeight();

        Ellipse2D.Double ellipse = new Ellipse2D.Double(0,0, x, y);
        g2.draw(ellipse);
        g2.setColor(Color.GREEN);
        g2.fill(ellipse);
    }
}
