import javax.swing.*;

public class EllipseViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(300,400);
        frame.setTitle("Ellipse - E3.17");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //add component to frame
        EllipseComponent component = new EllipseComponent();
        frame.add(component);

        //make frame visible
        frame.setVisible(true);
    }

}
